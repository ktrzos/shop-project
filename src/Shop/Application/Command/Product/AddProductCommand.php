<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Brick\Money\Money;
use Shop\Domain\Entity\ProductId;

final readonly class AddProductCommand
{
    public function __construct(
        public ProductId $id,
        public string $title,
        public Money $price,
    ) {
    }
}
