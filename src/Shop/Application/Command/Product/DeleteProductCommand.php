<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Shop\Domain\Entity\ProductId;

final readonly class DeleteProductCommand
{
    public function __construct(public ProductId $id)
    {
    }
}
