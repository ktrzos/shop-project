<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Shared\Application\Command\CommandHandlerInterface;
use Shared\Application\EventDispatcher\DomainEventDispatcherInterface;
use Shop\Domain\Entity\Product;
use Shop\Domain\Repository\ProductRepositoryInterface;

final readonly class AddProductCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private DomainEventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function __invoke(AddProductCommand $command): void
    {
        $entity = new Product(
            id: $command->id,
            title: $command->title,
            price: $command->price,
        );

        $this->productRepository->save($entity);
        $this->eventDispatcher->dispatchAll($entity);
    }
}
