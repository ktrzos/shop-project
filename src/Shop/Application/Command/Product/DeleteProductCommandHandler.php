<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Shared\Application\Command\CommandHandlerInterface;
use Shared\Application\EventDispatcher\DomainEventDispatcherInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Repository\ProductRepositoryInterface;
use Shop\Domain\Service\ProductDeleteService;

final readonly class DeleteProductCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private ProductDeleteService $productDeleteService,
        private DomainEventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function __invoke(DeleteProductCommand $command): void
    {
        $product = $this->productRepository->findById($command->id);

        if ($product === null) {
            throw new NotFoundException();
        }

        $product->delete($this->productDeleteService);

        foreach ($product->pullDomainEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
    }
}
