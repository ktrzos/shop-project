<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Shared\Application\Command\CommandHandlerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Repository\ProductRepositoryInterface;

final readonly class UpdateProductCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
    ) {
    }

    public function __invoke(UpdateProductCommand $command): void
    {
        $product = $this->productRepository->findById($command->id);

        if ($product === null) {
            throw new NotFoundException();
        }

        $product->updateProduct($command->title, $command->price);
        $this->productRepository->save($product);
    }
}
