<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Shared\Application\Command\CommandHandlerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Repository\CartRepositoryInterface;
use Shop\Domain\Repository\ProductRepositoryInterface;

final readonly class RemoveProductFromCartCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private CartRepositoryInterface $cartRepository,
    ) {
    }

    public function __invoke(RemoveProductFromCartCommand $command): void
    {
        $cart = $this->cartRepository->findById($command->cartId);
        $product = $this->productRepository->findById($command->productId);

        if ($cart === null || $product === null) {
            throw new NotFoundException();
        }

        $cart->removeProduct($product);
        $this->cartRepository->save($cart);
    }
}
