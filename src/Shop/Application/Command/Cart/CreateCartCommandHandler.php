<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Shared\Application\Command\CommandHandlerInterface;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Repository\CartRepositoryInterface;

final readonly class CreateCartCommandHandler implements CommandHandlerInterface
{
    public function __construct(private CartRepositoryInterface $cartRepository)
    {
    }

    public function __invoke(CreateCartCommand $command): void
    {
        $cart = new Cart(id: $command->id);

        $this->cartRepository->save($cart);
    }
}
