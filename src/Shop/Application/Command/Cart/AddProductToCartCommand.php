<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\ProductId;

final readonly class AddProductToCartCommand
{
    public function __construct(
        public CartId $cartId,
        public ProductId $productId,
    ) {
    }
}
