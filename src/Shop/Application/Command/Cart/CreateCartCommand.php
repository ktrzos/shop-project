<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Shop\Domain\Entity\CartId;

final readonly class CreateCartCommand
{
    public function __construct(public CartId $id)
    {
    }
}
