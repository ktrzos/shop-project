<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

use Shared\Application\Query\QueryHandlerInterface;
use Shop\Domain\View\ProductView;
use Shop\Domain\ViewRepository\ProductViewRepositoryInterface;

final readonly class GetProductByIdQueryHandler implements QueryHandlerInterface
{
    public function __construct(private ProductViewRepositoryInterface $productViewRepository)
    {
    }

    public function __invoke(GetProductByIdQuery $query): ?ProductView
    {
        return $this->productViewRepository->findById($query->id);
    }
}
