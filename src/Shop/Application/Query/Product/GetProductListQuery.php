<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

final class GetProductListQuery
{
    public function __construct(public ?int $page)
    {
    }
}
