<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

use loophp\collection\Contract\Collection as CollectionInterface;
use Shared\Application\Query\QueryHandlerInterface;
use Shop\Domain\View\ProductView;
use Shop\Domain\ViewRepository\ProductViewRepositoryInterface;

final readonly class GetProductListQueryHandler implements QueryHandlerInterface
{
    private const ELEMENTS_PER_PAGE = 3;

    public function __construct(private ProductViewRepositoryInterface $productRepository)
    {
    }

    /**
     * @return CollectionInterface<int, ProductView>
     */
    public function __invoke(GetProductListQuery $query): CollectionInterface
    {
        return $this->productRepository->findAllForList(
            page: $query->page,
            elements_per_page: self::ELEMENTS_PER_PAGE,
        );
    }
}
