<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

use Shop\Domain\Entity\ProductId;

final readonly class GetProductByIdQuery
{
    public function __construct(public ProductId $id)
    {
    }
}
