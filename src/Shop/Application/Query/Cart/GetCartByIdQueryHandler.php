<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Shared\Application\Query\QueryHandlerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\View\CartView;
use Shop\Domain\ViewRepository\CartViewRepositoryInterface;

final readonly class GetCartByIdQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private CartViewRepositoryInterface $cartViewRepository,
    ) {
    }

    public function __invoke(GetCartByIdQuery $query): CartView
    {
        $cartView = $this->cartViewRepository->findById($query->id);

        if ($cartView === null) {
            throw new NotFoundException();
        }

        return $cartView;
    }
}
