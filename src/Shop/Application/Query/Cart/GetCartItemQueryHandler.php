<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Shared\Application\Query\QueryHandlerInterface;
use Shop\Domain\View\CartItemView;
use Shop\Domain\ViewRepository\CartItemViewRepositoryInterface;

final readonly class GetCartItemQueryHandler implements QueryHandlerInterface
{
    public function __construct(private CartItemViewRepositoryInterface $cartItemViewRepository)
    {
    }

    public function __invoke(GetCartItemQuery $query): ?CartItemView
    {
        return $this->cartItemViewRepository->findCartItemView(
            $query->cartId,
            $query->productId,
        );
    }
}
