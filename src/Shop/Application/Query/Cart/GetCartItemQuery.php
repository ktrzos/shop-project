<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\ProductId;

final readonly class GetCartItemQuery
{
    public function __construct(public CartId $cartId, public ProductId $productId)
    {
    }
}
