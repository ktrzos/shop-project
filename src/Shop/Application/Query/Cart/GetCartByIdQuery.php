<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Shop\Domain\Entity\CartId;

final readonly class GetCartByIdQuery
{
    public function __construct(public CartId $id)
    {
    }
}
