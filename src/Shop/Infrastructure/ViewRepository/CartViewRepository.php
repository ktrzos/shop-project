<?php

declare(strict_types=1);

namespace Shop\Infrastructure\ViewRepository;

use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Service\CartTotalCalculator;
use Shop\Domain\View\CartView;
use Shop\Domain\ViewRepository\CartItemViewRepositoryInterface;
use Shop\Domain\ViewRepository\CartViewRepositoryInterface;

final readonly class CartViewRepository implements CartViewRepositoryInterface
{
    public function __construct(
        private CartItemViewRepositoryInterface $cartItemViewRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function findById(CartId $cartId): ?CartView
    {
        $cart = $this->entityManager->find(Cart::class, $cartId->value);

        if ($cart === null) {
            return null;
        }

        $itemCollection = $this->cartItemViewRepository->findListByCartId($cartId);

        return new CartView(
            id: $cartId,
            itemCollection: $itemCollection,
            totalPrice: CartTotalCalculator::calculateTotal($itemCollection),
        );
    }
}
