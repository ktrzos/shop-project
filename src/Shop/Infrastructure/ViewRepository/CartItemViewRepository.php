<?php

declare(strict_types=1);

namespace Shop\Infrastructure\ViewRepository;

use Brick\Money\Money;
use Doctrine\DBAL\Connection;
use loophp\collection\Collection;
use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\CartItemId;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\View\CartItemView;
use Shop\Domain\View\ProductView;
use Shop\Domain\ViewRepository\CartItemViewRepositoryInterface;
use Webmozart\Assert\Assert;

final readonly class CartItemViewRepository implements CartItemViewRepositoryInterface
{
    public function __construct(
        private Connection $connection,
    ) {
    }

    public function findCartItemView(CartId $cartId, ProductId $productId): ?CartItemView
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('i.id, i.amount, i.product_id, p.title, p.price, p.price_currency');
        $qb->from('cart_item', 'i');
        $qb->join('i', 'product', 'p', 'p.id = i.product_id');
        $qb->andWhere('i.cart_id = :cart_id');
        $qb->andWhere('i.product_id = :product_id');
        $qb->setParameter('cart_id', $cartId->value->toBinary());
        $qb->setParameter('product_id', $productId->value->toBinary());

        $row = $qb->fetchAssociative();

        if (false === $row) {
            return null;
        }

        Assert::string($row['id']);
        Assert::integer($row['amount']);
        Assert::string($row['product_id']);
        Assert::string($row['title']);
        Assert::integer($row['price']);
        Assert::string($row['price_currency']);

        return new CartItemView(
            cartItemId: CartItemId::fromString($row['id']),
            product: new ProductView(
                id: ProductId::fromString($row['product_id']),
                title: $row['title'],
                price: Money::ofMinor($row['price'], $row['price_currency']),
            ),
            quantity: $row['amount'],
        );
    }

    /**
     * @return CollectionInterface<int, CartItemView>
     */
    public function findListByCartId(CartId $id): CollectionInterface
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('i.id, i.product_id, amount, p.title, p.price, p.price_currency');
        $qb->from('cart_item', 'i');
        $qb->join('i', 'product', 'p', 'p.id = i.product_id');
        $qb->andWhere('cart_id = :cart_id');
        $qb->setParameter('cart_id', $id->value->toBinary());

        $result = $qb->fetchAllAssociative();

        return Collection::fromIterable($result)
            ->map(static function (array $row) {
                Assert::string($row['id']);
                Assert::string($row['product_id']);
                Assert::integer($row['amount']);
                Assert::string($row['title']);
                Assert::integer($row['price']);
                Assert::string($row['price_currency']);

                return new CartItemView(
                    cartItemId: CartItemId::fromString($row['id']),
                    product: new ProductView(
                        id: ProductId::fromString($row['product_id']),
                        title: $row['title'],
                        price: Money::ofMinor($row['price'], $row['price_currency']),
                    ),
                    quantity: $row['amount'],
                );
            });
    }
}
