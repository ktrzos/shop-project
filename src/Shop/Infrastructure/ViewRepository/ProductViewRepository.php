<?php

declare(strict_types=1);

namespace Shop\Infrastructure\ViewRepository;

use Brick\Money\Money;
use Doctrine\DBAL\Connection;
use loophp\collection\Collection;
use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\View\ProductView;
use Shop\Domain\ViewRepository\ProductViewRepositoryInterface;
use Webmozart\Assert\Assert;

final readonly class ProductViewRepository implements ProductViewRepositoryInterface
{
    public function __construct(
        private Connection $connection,
    ) {
    }

    /**
     * @return CollectionInterface<int, ProductView>
     */
    public function findAllForList(?int $page = null, ?int $elements_per_page = null): CollectionInterface
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->select('id, title, price, price_currency');
        $queryBuilder->from('product');

        if (null !== $elements_per_page) {
            $queryBuilder->setMaxResults($elements_per_page);
        }

        if (null !== $page) {
            $queryBuilder->setFirstResult($elements_per_page * ($page - 1));
        }

        return Collection::fromIterable($queryBuilder->fetchAllAssociative())
            ->map(static function (array $row) {
                $id = $row['id'];
                $title = $row['title'];
                $price = $row['price'];
                $currency = $row['price_currency'];

                Assert::string($id);
                Assert::string($title);
                Assert::integer($price);
                Assert::string($currency);

                return new ProductView(
                    id: ProductId::fromString($id),
                    title: $title,
                    price: Money::ofMinor($price, $currency),
                );
            });
    }

    public function findById(ProductId $id): ?ProductView
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->select('title, price, price_currency');
        $queryBuilder->from('product');
        $queryBuilder->andWhere('id = :id');
        $queryBuilder->setParameter('id', $id->value->toBinary());

        $row = $queryBuilder->fetchAssociative();

        if (false === $row) {
            return null;
        }

        $title = $row['title'];
        $price = $row['price'];
        $currency = $row['price_currency'];

        Assert::string($title);
        Assert::integer($price);
        Assert::string($currency);

        return new ProductView(
            id: $id,
            title: $title,
            price: Money::ofMinor($price, $currency),
        );
    }
}
