<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Application\Command\Product\AddProductCommand;
use Shop\Application\Command\Product\DeleteProductCommand;
use Shop\Application\Command\Product\UpdateProductCommand;
use Shop\Application\Query\Product\GetProductByIdQuery;
use Shop\Application\Query\Product\GetProductListQuery;
use Shop\Domain\Entity\ProductId;
use Shop\Infrastructure\Controller\RequestPayload\AddProductRequestPayload;
use Shop\Infrastructure\Controller\RequestPayload\UpdateProductRequestPayload;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

final class ProductController extends AbstractFOSRestController
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator,
    ) {
    }

    #[Route('/api/shop/product/list/{page}', name: 'product_list', requirements: ['page' => '\d+'], methods: ['GET'])]
    public function getProduct(?int $page = null): Response
    {
        $handledStamp = $this->messageBus
            ->dispatch(new GetProductListQuery($page))
            ->last(HandledStamp::class);

        Assert::notNull($handledStamp);

        $data = $handledStamp->getResult();

        Assert::isInstanceOf($data, CollectionInterface::class);

        if ($page !== null && $page > 1 && $data->isEmpty()) {
            throw $this->createNotFoundException();
        }

        return $this->handleView($this->view($data, Response::HTTP_OK));
    }

    #[Route('/api/shop/product', name: 'product_add', methods: ['POST'])]
    public function addProduct(Request $request): Response
    {
        $payload = $this->serializer->deserialize(
            $request->getContent(),
            AddProductRequestPayload::class,
            'json',
        );

        if ($this->validator->validate($payload)->count() > 0) {
            return $this->handleView($this->view('Invalid request payload.', Response::HTTP_BAD_REQUEST));
        }

        $id = ProductId::create();
        $this->messageBus->dispatch(new AddProductCommand(
            id: $id,
            title: $payload->requireTitle(),
            price: $payload->requirePrice(),
        ));

        $handledStamp = $this->messageBus->dispatch(new GetProductByIdQuery($id))->last(HandledStamp::class);
        Assert::notNull($handledStamp);
        $product = $handledStamp->getResult();

        return $this->handleView($this->view($product, Response::HTTP_OK));
    }

    #[Route('/api/shop/product/{id}', name: 'product_delete', methods: ['DELETE'])]
    public function deleteProduct(ProductId $id): Response
    {
        $this->messageBus->dispatch(new DeleteProductCommand($id));

        return $this->handleView($this->view(true, Response::HTTP_OK));
    }

    #[Route('/api/shop/product/{id}', name: 'product_update', methods: ['PUT'])]
    public function updateProduct(Request $request, ProductId $id): Response
    {
        $payload = $this->serializer->deserialize(
            $request->getContent(),
            UpdateProductRequestPayload::class,
            'json',
        );

        if ($this->validator->validate($payload)->count() > 0) {
            return $this->handleView($this->view('Invalid request payload.', Response::HTTP_BAD_REQUEST));
        }

        $this->messageBus->dispatch(new UpdateProductCommand(
            id: $id,
            title: $payload->getTitle(),
            price: $payload->getPrice(),
        ));

        return $this->handleView($this->view(true, Response::HTTP_OK));
    }
}
