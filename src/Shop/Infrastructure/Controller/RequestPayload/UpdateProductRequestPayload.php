<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Controller\RequestPayload;

use Brick\Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

final readonly class UpdateProductRequestPayload
{
    public function __construct(
        #[Assert\Type('string')]
        private mixed $title = null,
        #[Assert\Type('float')]
        #[Assert\GreaterThan(0)]
        private mixed $price = null,
    ) {
    }

    public function getTitle(): ?string
    {
        \Webmozart\Assert\Assert::nullOrString($this->title);

        return $this->title;
    }

    public function getPrice(): ?Money
    {
        \Webmozart\Assert\Assert::nullOrFloat($this->price);

        if ($this->price === null) {
            return null;
        }

        return Money::ofMinor(floor($this->price * 100), 'USD');
    }
}
