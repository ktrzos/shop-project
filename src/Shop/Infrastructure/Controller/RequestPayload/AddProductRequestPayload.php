<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Controller\RequestPayload;

use Brick\Money\Money;
use Symfony\Component\Validator\Constraints as Assert;

final readonly class AddProductRequestPayload
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Type('string')]
        private mixed $title = null,
        #[Assert\NotBlank]
        #[Assert\Type('float')]
        #[Assert\GreaterThan(0)]
        private mixed $price = null,
    ) {
    }

    public function requireTitle(): string
    {
        \Webmozart\Assert\Assert::string($this->title);

        return $this->title;
    }

    public function requirePrice(): Money
    {
        \Webmozart\Assert\Assert::float($this->price);

        return Money::ofMinor(floor($this->price * 100), 'USD');
    }
}
