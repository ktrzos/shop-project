<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Shop\Application\Command\Cart\AddProductToCartCommand;
use Shop\Application\Command\Cart\CreateCartCommand;
use Shop\Application\Command\Cart\RemoveProductFromCartCommand;
use Shop\Application\Query\Cart\GetCartByIdQuery;
use Shop\Application\Query\Cart\GetCartItemQuery;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\ProductId;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

final class CartController extends AbstractFOSRestController
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
    ) {
    }

    #[Route('/api/shop/cart/{cartId}', name: 'cart_get', methods: ['GET'])]
    public function getCart(CartId $cartId): Response
    {
        $handledStamp = $this->messageBus
            ->dispatch(new GetCartByIdQuery(id: $cartId))
            ->last(HandledStamp::class);

        Assert::notNull($handledStamp);

        $data = $handledStamp->getResult();

        return $this->handleView($this->view($data, Response::HTTP_OK));
    }

    #[Route('/api/shop/cart', name: 'cart_create', methods: ['POST'])]
    public function createCart(): Response
    {
        $cartId = CartId::create();

        $this->messageBus->dispatch(new CreateCartCommand(id: $cartId));

        return $this->handleView($this->view($cartId->value, Response::HTTP_OK));
    }

    #[Route('/api/shop/cart/add_product/{cartId}/{productId}', name: 'cart_add_product', methods: ['PUT'])]
    public function addProductToCart(CartId $cartId, ProductId $productId): Response
    {
        $this->messageBus->dispatch(new AddProductToCartCommand($cartId, $productId));
        $handledStamp = $this->messageBus
            ->dispatch(new GetCartItemQuery($cartId, $productId))
            ->last(HandledStamp::class);

        Assert::notNull($handledStamp);

        $data = $handledStamp->getResult();

        return $this->handleView($this->view($data, Response::HTTP_OK));
    }

    #[Route('/api/shop/cart/remove_product/{cartId}/{productId}', name: 'cart_remove_product', methods: ['DELETE'])]
    public function removeProductFromCart(CartId $cartId, ProductId $productId): Response
    {
        $this->messageBus->dispatch(new RemoveProductFromCartCommand($cartId, $productId));

        return $this->handleView($this->view(true, Response::HTTP_OK));
    }
}
