<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Repository\CartRepositoryInterface;

final readonly class CartRepository implements CartRepositoryInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function findById(CartId $id): ?Cart
    {
        return $this->entityManager->find(Cart::class, $id->value);
    }

    public function save(Cart $cart): void
    {
        $this->entityManager->persist($cart);
        $this->entityManager->flush();
    }
}
