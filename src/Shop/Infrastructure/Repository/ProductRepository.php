<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\Repository\ProductRepositoryInterface;
use Webmozart\Assert\Assert;

final readonly class ProductRepository implements ProductRepositoryInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function findById(ProductId $id): ?Product
    {
        return $this->entityManager->find(Product::class, $id->value);
    }

    public function count(): int
    {
        $queryBuilder = $this->entityManager->getConnection()->createQueryBuilder();
        $queryBuilder->select('COUNT(*)');
        $queryBuilder->from('product');

        $result = $queryBuilder->fetchOne();

        Assert::integer($result);

        return $result;
    }

    public function delete(Product $product): void
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    public function save(Product $product): void
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}
