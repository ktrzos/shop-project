<?php

declare(strict_types=1);

namespace Shop\Infrastructure\ValueResolver;

use Shop\Domain\Entity\CartId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class CartIdValueResolver implements ValueResolverInterface
{
    /**
     * @return iterable<CartId>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argumentType = $argument->getType();

        if ($argumentType !== CartId::class) {
            return [];
        }

        $value = $request->attributes->get($argument->getName());

        if (!is_string($value)) {
            return [];
        }

        return [CartId::fromString($value)];
    }
}
