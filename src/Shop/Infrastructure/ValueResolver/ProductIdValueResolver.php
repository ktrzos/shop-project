<?php

declare(strict_types=1);

namespace Shop\Infrastructure\ValueResolver;

use Shop\Domain\Entity\ProductId;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class ProductIdValueResolver implements ValueResolverInterface
{
    /**
     * @return iterable<ProductId>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argumentType = $argument->getType();

        if ($argumentType !== ProductId::class) {
            return [];
        }

        $value = $request->attributes->get($argument->getName());

        if (!is_string($value)) {
            return [];
        }

        return [ProductId::fromString($value)];
    }
}
