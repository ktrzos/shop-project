<?php

declare(strict_types=1);

namespace Shop\Infrastructure\DataFixture;

use Brick\Money\Currency;
use Brick\Money\Money;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;

final class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        self::createProductFallout($manager);
        self::createProductDontStarve($manager);
        self::createProductBaldursGate($manager);
        self::createProductIcewindDale($manager);
        self::createProductBloodborne($manager);

        $manager->flush();
    }

    private static function createProductFallout(ObjectManager $manager): void
    {
        $product1 = new Product(
            id: ProductId::create(),
            title: 'Fallout',
            price: Money::of('1.99', Currency::of('USD')),
        );

        $manager->persist($product1);
    }

    private static function createProductDontStarve(ObjectManager $manager): void
    {
        $product1 = new Product(
            id: ProductId::create(),
            title: 'Don’t Starve',
            price: Money::of('2.99', Currency::of('USD')),
        );

        $manager->persist($product1);
    }

    private static function createProductBaldursGate(ObjectManager $manager): void
    {
        $product1 = new Product(
            id: ProductId::create(),
            title: 'Baldur’s Gate',
            price: Money::of('3.99', Currency::of('USD')),
        );

        $manager->persist($product1);
    }

    private static function createProductIcewindDale(ObjectManager $manager): void
    {
        $product1 = new Product(
            id: ProductId::create(),
            title: 'Icewind Dale',
            price: Money::of('4.99', Currency::of('USD')),
        );

        $manager->persist($product1);
    }

    private static function createProductBloodborne(ObjectManager $manager): void
    {
        $product1 = new Product(
            id: ProductId::create(),
            title: 'Bloodborne',
            price: Money::of('5.99', Currency::of('USD')),
        );

        $manager->persist($product1);
    }
}
