<?php

declare(strict_types=1);

namespace Shop\Domain\ViewRepository;

use Shop\Domain\Entity\CartId;
use Shop\Domain\View\CartView;

interface CartViewRepositoryInterface
{
    public function findById(CartId $cartId): ?CartView;
}
