<?php

declare(strict_types=1);

namespace Shop\Domain\ViewRepository;

use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\View\CartItemView;

interface CartItemViewRepositoryInterface
{
    /**
     * @return CollectionInterface<int, CartItemView>
     */
    public function findListByCartId(CartId $id): CollectionInterface;

    public function findCartItemView(CartId $cartId, ProductId $productId): ?CartItemView;
}
