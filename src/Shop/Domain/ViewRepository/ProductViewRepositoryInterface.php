<?php

declare(strict_types=1);

namespace Shop\Domain\ViewRepository;

use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\View\ProductView;

interface ProductViewRepositoryInterface
{
    public function findById(ProductId $id): ?ProductView;

    /**
     * @return CollectionInterface<int, ProductView>
     */
    public function findAllForList(?int $page = null, ?int $elements_per_page = null): CollectionInterface;
}
