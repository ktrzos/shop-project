<?php

declare(strict_types=1);

namespace Shop\Domain\Repository;

use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;

interface CartRepositoryInterface
{
    public function save(Cart $cart): void;

    public function findById(CartId $id): ?Cart;
}
