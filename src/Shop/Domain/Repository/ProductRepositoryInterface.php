<?php

declare(strict_types=1);

namespace Shop\Domain\Repository;

use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;

interface ProductRepositoryInterface
{
    public function delete(Product $product): void;

    public function save(Product $product): void;

    public function findById(ProductId $id): ?Product;

    public function count(): int;
}
