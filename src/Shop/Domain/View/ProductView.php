<?php

declare(strict_types=1);

namespace Shop\Domain\View;

use Brick\Money\Money;
use Shop\Domain\Entity\ProductId;

final readonly class ProductView
{
    public function __construct(
        public ProductId $id,
        public string $title,
        public Money $price,
    ) {
    }
}
