<?php

declare(strict_types=1);

namespace Shop\Domain\View;

use Brick\Money\Money;
use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\Entity\CartId;

final readonly class CartView
{
    /**
     * @param CollectionInterface<int, CartItemView> $itemCollection
     */
    public function __construct(
        public CartId $id,
        public CollectionInterface $itemCollection,
        public Money $totalPrice,
    ) {
    }
}
