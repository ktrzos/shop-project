<?php

declare(strict_types=1);

namespace Shop\Domain\View;

use Shop\Domain\Entity\CartItemId;

final readonly class CartItemView
{
    public function __construct(
        public CartItemId $cartItemId,
        public ProductView $product,
        public int $quantity,
    ) {
    }
}
