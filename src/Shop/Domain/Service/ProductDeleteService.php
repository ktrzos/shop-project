<?php

declare(strict_types=1);

namespace Shop\Domain\Service;

use Shop\Domain\Entity\Product;
use Shop\Domain\Exception\ProductCannotBeDeletedException;
use Shop\Domain\Repository\ProductRepositoryInterface;

final readonly class ProductDeleteService
{
    private const MIN_AMOUNT_OF_PRODUCTS = 5;

    public function __construct(
        private ProductRepositoryInterface $productRepository,
    ) {
    }

    public function delete(Product $product): void
    {
        $this->checkTheMinimalAmountOfProducts();
        $this->productRepository->delete($product);
    }

    public function checkTheMinimalAmountOfProducts(): void
    {
        if ($this->productRepository->count() === self::MIN_AMOUNT_OF_PRODUCTS) {
            throw new ProductCannotBeDeletedException();
        }
    }
}
