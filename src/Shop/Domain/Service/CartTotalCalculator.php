<?php

declare(strict_types=1);

namespace Shop\Domain\Service;

use Brick\Money\Money;
use loophp\collection\Contract\Collection as CollectionInterface;
use Shop\Domain\View\CartItemView;

final class CartTotalCalculator
{
    /**
     * @param CollectionInterface<int, CartItemView> $itemCollection
     */
    public static function calculateTotal(CollectionInterface $itemCollection): Money
    {
        return $itemCollection
            ->map(static fn (CartItemView $item) => $item->product->price->multipliedBy($item->quantity))
            ->reduce(static fn (Money $acc, Money $item) => $acc->plus($item), Money::of(0, 'USD'));
    }
}
