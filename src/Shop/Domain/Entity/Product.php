<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Brick\Money\Money;
use InvalidArgumentException;
use Shared\Domain\Aggregate\AggregateRoot;
use Shop\Domain\Event\ProductCreated;
use Shop\Domain\Event\ProductDeleted;
use Shop\Domain\Service\ProductDeleteService;
use Symfony\Component\Uid\Ulid;

class Product extends AggregateRoot
{
    private Ulid $id;
    private string $title;
    private int $price;
    private string $priceCurrency;

    public function __construct(
        ProductId $id,
        string $title,
        Money $price,
    ) {
        $this->checkPrice($price);

        $this->id = $id->value;
        $this->title = $title;
        $this->setPrice($price);

        $this->recordDomainEvent(new ProductCreated($id, $title));
    }

    public function getId(): ProductId
    {
        return ProductId::fromString((string) $this->id);
    }

    public function delete(ProductDeleteService $service): void
    {
        $service->delete($this);
        $this->recordDomainEvent(new ProductDeleted($this->getId()));
    }

    public function updateProduct(
        ?string $title,
        ?Money $price,
    ): void {
        if ($title !== null) {
            $this->title = $title;
        }

        if ($price !== null) {
            $this->checkPrice($price);
            $this->setPrice($price);
        }
    }

    private function setPrice(Money $price): void
    {
        $this->price = $price->getMinorAmount()->toInt();
        $this->priceCurrency = $price->getCurrency()->getCurrencyCode();
    }

    private function checkPrice(Money $price): void
    {
        if ($price->getCurrency()->is('USD') === false) {
            throw new InvalidArgumentException('Only USD currency is allowed');
        }
    }
}
