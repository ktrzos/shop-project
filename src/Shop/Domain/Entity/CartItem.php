<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Shop\Domain\Exception\CartItemAmountLimitReachedException;
use Symfony\Component\Uid\Ulid;

class CartItem
{
    private const MAX_AMOUNT = 10;

    private readonly Ulid $id;

    public function __construct(
        CartItemId $id,
        private ?Cart $cart,
        private Product $product,
        private int $amount = 1,
    ) {
        $this->id = $id->value;
    }

    public function increaseQuantity(): void
    {
        if ($this->amount >= self::MAX_AMOUNT) {
            throw CartItemAmountLimitReachedException::create();
        }

        ++$this->amount;
    }

    public function getProductId(): ProductId
    {
        return $this->product->getId();
    }
}
