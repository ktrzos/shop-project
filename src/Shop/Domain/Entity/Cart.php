<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Shared\Domain\Aggregate\AggregateRoot;
use Shop\Domain\Exception\CartLimitReachedException;
use Symfony\Component\Uid\Ulid;

class Cart extends AggregateRoot
{
    private const MAX_ITEM_COUNT_IN_CART = 3;

    private readonly Ulid $id;

    /** @var Collection<int, CartItem> */
    private Collection $itemList;

    public function __construct(
        CartId $id,
    ) {
        $this->id = $id->value;
        $this->itemList = new ArrayCollection();
    }

    public function getId(): CartId
    {
        return CartId::fromString((string) $this->id);
    }

    public function addProduct(Product $product): void
    {
        $item = $this->findItemForProduct($product);

        if ($item === null) {
            $this->createNewCartItem($product);
        } else {
            $item->increaseQuantity();
        }
    }

    public function removeProduct(Product $product): void
    {
        $item = $this->findItemForProduct($product);

        if (null !== $item) {
            $this->itemList->removeElement($item);
        }
    }

    private function findItemForProduct(Product $product): null | CartItem
    {
        $productId = (string) $product->getId()->value;

        foreach ($this->itemList->toArray() as $item) {
            $itemId = (string) $item->getProductId()->value;

            if ($itemId === $productId) {
                return $item;
            }
        }

        return null;
    }

    private function createNewCartItem(Product $product): void
    {
        if ($this->itemList->count() >= self::MAX_ITEM_COUNT_IN_CART) {
            throw CartLimitReachedException::create();
        }

        $this->itemList->add(
            new CartItem(
                id: CartItemId::create(),
                cart: $this,
                product: $product,
            ),
        );
    }
}
