<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Shared\Domain\ValueObject\Id;
use Stringable;

final readonly class CartId implements Stringable
{
    use Id;
}
