<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Shared\Domain\Aggregate\AggregateRootId;

final readonly class ProductId extends AggregateRootId
{
}
