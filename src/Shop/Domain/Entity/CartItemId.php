<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Shared\Domain\ValueObject\Id;

final readonly class CartItemId
{
    use Id;
}
