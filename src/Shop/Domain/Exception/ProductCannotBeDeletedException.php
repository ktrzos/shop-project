<?php

declare(strict_types=1);

namespace Shop\Domain\Exception;

use LogicException;

final class ProductCannotBeDeletedException extends LogicException
{
}
