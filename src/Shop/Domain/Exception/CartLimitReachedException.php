<?php

declare(strict_types=1);

namespace Shop\Domain\Exception;

use LogicException;

final class CartLimitReachedException extends LogicException
{
    private function __construct(string $message = '')
    {
        parent::__construct($message);
    }

    public static function create(): self
    {
        return new self('Cart limit reached.');
    }
}
