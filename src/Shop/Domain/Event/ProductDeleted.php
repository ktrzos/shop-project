<?php

declare(strict_types=1);

namespace Shop\Domain\Event;

use Shared\Domain\Event\DomainEventInterface;
use Shop\Domain\Entity\ProductId;

final readonly class ProductDeleted implements DomainEventInterface
{
    public function __construct(
        public ProductId $id,
    ) {
    }
}
