<?php

declare(strict_types=1);

namespace Shop\Domain\Event;

use Shared\Domain\Event\DomainEventInterface;
use Shop\Domain\Entity\ProductId;

final readonly class ProductCreated implements DomainEventInterface
{
    public function __construct(
        public ProductId $id,
        public string $title,
    ) {
    }
}
