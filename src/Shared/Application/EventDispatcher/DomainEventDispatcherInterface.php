<?php

declare(strict_types=1);

namespace Shared\Application\EventDispatcher;

use Shared\Domain\Aggregate\AggregateRoot;
use Shared\Domain\Event\DomainEventInterface;

interface DomainEventDispatcherInterface
{
    public function dispatch(DomainEventInterface $event): void;

    public function dispatchAll(AggregateRoot $root): void;
}
