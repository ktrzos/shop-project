<?php

declare(strict_types=1);

namespace Shared\Infrastructure;

use Shared\Application\EventDispatcher\DomainEventDispatcherInterface;
use Shared\Domain\Aggregate\AggregateRoot;
use Shared\Domain\Event\DomainEventInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final readonly class DomainEventDispatcher implements DomainEventDispatcherInterface
{
    public function __construct(private EventDispatcherInterface $eventDispatcher)
    {
    }

    public function dispatch(DomainEventInterface $event): void
    {
        $this->eventDispatcher->dispatch($event);
    }

    public function dispatchAll(AggregateRoot $root): void
    {
        foreach ($root->pullDomainEvents() as $event) {
            $this->eventDispatcher->dispatch($event);
        }
    }
}
