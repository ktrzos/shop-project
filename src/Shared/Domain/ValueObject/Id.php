<?php

declare(strict_types=1);

namespace Shared\Domain\ValueObject;

use Symfony\Component\Uid\Ulid;

trait Id
{
    final private function __construct(public readonly Ulid $value)
    {
    }

    final public static function create(): static
    {
        return new static(new Ulid());
    }

    final public static function fromString(string $value): static
    {
        return new static(Ulid::fromString($value));
    }

    final public function __toString(): string
    {
        return (string) $this->value;
    }
}
