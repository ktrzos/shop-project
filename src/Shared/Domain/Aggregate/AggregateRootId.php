<?php

declare(strict_types=1);

namespace Shared\Domain\Aggregate;

use Shared\Domain\ValueObject\Id;
use Stringable;

abstract readonly class AggregateRootId implements Stringable
{
    use Id;
}
