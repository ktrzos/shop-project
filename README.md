# Shop Project

## Run the project

- Execute `docker/start.sh` to run the projects environment
- Install dependencies `bin/composer install`
- Run `sudo chmod -R 0777 var` if permissions to the `var` directory will be invalid
- Create database `bin/symfony doctrine:database:create`
- Create schema `bin/symfony doctrine:migrations:migrate`
- Run Doctrine fixtures `bin/symfony doctrine:fixtures:load`
- Project should be available via https://localhost/
- Run `docker/stop.sh` to stop Docker containers

## REST API

- `/api/shop/product/list/{page<\d+>?1}` [GET]
- `/api/shop/product` [POST]
  - request payload JSON:
    - title: string
    - price: float
- `/api/shop/product/{id}` [DELETE]
- `/api/shop/product/{id}` [PUT]
  - request payload JSON:
    - title?: string
    - price?: float
- `/api/shop/cart/{cartId}` [GET]
- `/api/shop/cart` [POST] 
- `/api/shop/cart/add_product/{cartId}/{productId}` [PUT]
- `/api/shop/cart/remove_product/{cartId}/{productId}` [DELETE]

## Additional tools

- `bin/symfony` - run Symfony console from inside a Docker container
- `bin/phpunit` - run all tests
- `bin/phpstan` - run PHPStan analysis
- `bin/coverage` - create the test coverage report 
  - Report URL: https://localhost/coverage/index.html
  - Dashboard: https://localhost/coverage/dashboard.html
- `bin/deptrack_bounded_context` - run Deptrack bounded context analysis
- `bin/deptrack_hexagonal` - run Deptrack hexagonal architecture analysis

## TODO

- Change the name of the `cart_item.amount` database table column onto `cart_item.quantity`.

## What can be improved

- Use "API Platform" instead of `friendsofsymfony/rest-bundle`
- Separate MessageBus onto QueryBus and CommandBus
- Better error handling for `NotFoundException`.
- Simplify the code for getting data with `MessageBus` (without having `HandledStamp` use in the controller action).
- Make some structures for creating resources to be used in tests (currently many methods like `createProduct(...)` are duplicated).
- Make request validation abstraction layer (for example for `addProduct` and `updateProduct` methods) to prettify code in there (attribute or value resolver?).