<?php

declare(strict_types=1);

namespace Test\Shop\Infrastructure\Controller;

use Doctrine\DBAL\Connection;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

final class ProductControllerTest extends WebTestCase
{
    /**
     * @return Generator{string}
     */
    public static function smokeTestPathProvider(): Generator
    {
        yield ['/api/shop/product/list'];
        yield ['/api/shop/product/list/1'];
        yield ['/api/shop/product/list/2'];
    }

    #[DataProvider('smokeTestPathProvider')]
    public function testSmoke(string $path): void
    {
        $client = self::createClient();
        $client->request('GET', $path);

        self::assertResponseIsSuccessful();
    }

    /**
     * @return Generator{mixed[]}
     */
    public static function addInvalidAddProductPayloadProvider(): Generator
    {
        yield 'empty' => [[]];
        yield 'empty_title' => [['title' => '', 'price' => 100.99]];
        yield 'empty_price' => [['title' => ':title:', 'price' => '']];
        yield 'no_title' => [['price' => 99.99]];
        yield 'no_price' => [['title' => ':title:']];
        yield 'price_as_string1' => [['title' => ':product_title:', 'price' => ':price:']];
        yield 'price_as_string2' => [['title' => ':product_title:', 'price' => '99.99']];
        yield 'price_as_string3' => [['title' => ':product_title:', 'price' => '99,99']];
        yield 'title_not_as_string' => [['title' => 1234, 'price' => 100.99]];
    }

    /**
     * @param mixed[] $payload
     */
    #[DataProvider('addInvalidAddProductPayloadProvider')]
    public function testAddingProductInvalidPayload(array $payload): void
    {
        $encoded_payload = json_encode($payload, JSON_THROW_ON_ERROR);

        $client = self::createClient();
        $client->request('POST', '/api/shop/product', content: $encoded_payload);
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
    }

    public function testProductAddAndRemove(): void
    {
        $payload = json_encode([
            'title' => ':product_title:',
            'price' => 100.99,
        ], JSON_THROW_ON_ERROR);

        $client = self::createClient();
        $client->request('POST', '/api/shop/product', content: $payload);
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $productId = $this->loadProductId(':product_title:');
        self::assertNotNull($productId);

        $client->request('DELETE', '/api/shop/product/' . $productId->value);
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        self::assertNull($this->loadProductId(':product_title:'));
    }

    public function testProductUpdate(): void
    {
        $payload = json_encode([
            'title' => ':product_title:',
            'price' => 100.99,
        ], JSON_THROW_ON_ERROR);

        $client = self::createClient();
        $client->request('POST', '/api/shop/product', content: $payload);
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $productId = $this->loadProductId(':product_title:');
        self::assertNotNull($productId);

        $payload_update = json_encode([
            'title' => ':updated_product_title:',
        ], JSON_THROW_ON_ERROR);

        $client->request('PUT', '/api/shop/product/' . $productId->value, content: $payload_update);
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        self::assertNull($this->loadProductId(':product_title:'));
        self::assertNotNull($this->loadProductId(':updated_product_title:'));
    }

    private function loadProductId(string $title): ?ProductId
    {
        /** @var Connection $connection */
        $connection = self::getContainer()->get(Connection::class);

        $qb = $connection->createQueryBuilder();
        $qb->select('id');
        $qb->from('product');
        $qb->where('title = :title');
        $qb->setParameter('title', $title);
        $qb->setMaxResults(1);

        $result = $qb->fetchOne();

        if ($result === false || !is_string($result)) {
            return null;
        }

        return ProductId::fromString($result);
    }
}
