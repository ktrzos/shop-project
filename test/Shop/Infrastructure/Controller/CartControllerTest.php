<?php

declare(strict_types=1);

namespace Shop\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

final class CartControllerTest extends WebTestCase
{
    public function testCreateAndGet(): void
    {
        $client = self::createClient();
        $client->request('POST', '/api/shop/cart');
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $response = $client->getResponse()->getContent();
        Assert::string($response);
        $cartId = json_decode($response, flags: JSON_THROW_ON_ERROR);

        $client->request('GET', '/api/shop/cart/' . $cartId);
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
