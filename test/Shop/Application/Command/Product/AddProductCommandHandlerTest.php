<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class AddProductCommandHandlerTest extends KernelTestCase
{
    private static EntityManagerInterface $em;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$em = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testInvoke(): void
    {
        $command = new AddProductCommand(
            id: ProductId::create(),
            title: ':title:',
            price: Money::of(155.99, 'USD'),
        );

        $handler = self::getContainer()->get(AddProductCommandHandler::class);
        $handler($command);

        $productList = self::$em->getRepository(Product::class)->findAll();

        self::assertCount(6, $productList);
    }
}
