<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Brick\Money\Money;
use Doctrine\DBAL\Connection;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class UpdateProductCommandHandlerTest extends KernelTestCase
{
    private static Connection $connection;
    private static UpdateProductCommandHandler $handler;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$connection = self::getContainer()->get(Connection::class);
        self::$handler = self::getContainer()->get(UpdateProductCommandHandler::class);
    }

    public function testInvokeNoDataProvided(): void
    {
        $initial_data = $this->loadFirstProduct();
        $productId = ProductId::fromString($initial_data['id']);
        $command = new UpdateProductCommand(id: $productId, title: null, price: null);

        (self::$handler)($command);

        $actual_data = $this->loadFirstProduct();

        self::assertEquals($actual_data['id'], $initial_data['id']);
        self::assertEquals($actual_data['title'], $initial_data['title']);
        self::assertEquals($actual_data['price'], $initial_data['price']);
    }

    public function testUpdateTitle(): void
    {
        $initial_data = $this->loadFirstProduct();
        $productId = ProductId::fromString($initial_data['id']);
        $command = new UpdateProductCommand(id: $productId, title: ':changed_title:', price: null);

        (self::$handler)($command);

        $actual_data = $this->loadFirstProduct();

        self::assertEquals($actual_data['id'], $initial_data['id']);
        self::assertNotEquals($actual_data['title'], $initial_data['title']);
        self::assertEquals(':changed_title:', $actual_data['title']);
        self::assertEquals($actual_data['price'], $initial_data['price']);
    }

    public function testUpdatePrice(): void
    {
        $initial_data = $this->loadFirstProduct();
        $productId = ProductId::fromString($initial_data['id']);
        $command = new UpdateProductCommand(id: $productId, title: null, price: Money::of(123.45, 'USD'));

        (self::$handler)($command);

        $actual_data = $this->loadFirstProduct();

        self::assertEquals($actual_data['id'], $initial_data['id']);
        self::assertEquals($actual_data['title'], $initial_data['title']);
        self::assertNotEquals($actual_data['price'], $initial_data['price']);
        self::assertEquals(12345, $actual_data['price']);
    }

    public function testUpdateNotExisting(): void
    {
        $this->expectException(NotFoundException::class);

        $command = new UpdateProductCommand(id: ProductId::create(), title: null, price: Money::of(123.45, 'USD'));
        (self::$handler)($command);
    }

    /**
     * @return array{id: string, price: int, title: string}
     */
    private function loadFirstProduct(): array
    {
        $qb = self::$connection->createQueryBuilder();
        $qb->select('id, price, title');
        $qb->from('product');
        $qb->setMaxResults(1);

        $result = $qb->fetchAssociative();

        Assert::notFalse($result);
        Assert::string($result['id']);
        Assert::integer($result['price']);
        Assert::string($result['title']);

        return $result;
    }
}
