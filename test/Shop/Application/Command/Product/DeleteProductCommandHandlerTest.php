<?php

declare(strict_types=1);

namespace Shop\Application\Command\Product;

use Doctrine\DBAL\Connection;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\Exception\ProductCannotBeDeletedException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class DeleteProductCommandHandlerTest extends KernelTestCase
{
    private static Connection $connection;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$connection = self::getContainer()->get(Connection::class);
    }

    public function testInvokeDeleteOnTheLimit(): void
    {
        $this->expectException(ProductCannotBeDeletedException::class);

        $qb = self::$connection->createQueryBuilder();
        $qb->select('id');
        $qb->from('product');
        $qb->setMaxResults(1);
        $id = $qb->fetchOne();

        Assert::string($id);

        $command = new DeleteProductCommand(id: ProductId::fromString($id));

        $handler = self::getContainer()->get(DeleteProductCommandHandler::class);
        $handler($command);
    }

    public function testInvokeDeleteOnNotExistingResource(): void
    {
        $this->expectException(NotFoundException::class);

        $command = new DeleteProductCommand(id: ProductId::create());

        $handler = self::getContainer()->get(DeleteProductCommandHandler::class);
        $handler($command);
    }
}
