<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CreateCartCommandHandlerTest extends KernelTestCase
{
    private static EntityManagerInterface $em;
    private static CreateCartCommandHandler $handler;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(CreateCartCommandHandler::class);
        self::$em = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testInvoke(): void
    {
        $id = CartId::create();

        (self::$handler)(new CreateCartCommand($id));

        $cart = self::$em->find(Cart::class, $id->value);

        self::assertNotNull($cart);
    }
}
