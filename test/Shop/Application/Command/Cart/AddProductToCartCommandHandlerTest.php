<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\Exception\CartLimitReachedException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class AddProductToCartCommandHandlerTest extends KernelTestCase
{
    private static AddProductToCartCommandHandler $handler;
    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(AddProductToCartCommandHandler::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testInvokeNotExistingCart(): void
    {
        $this->expectException(NotFoundException::class);

        $productId = self::createProduct();
        self::$entityManager->flush();

        (self::$handler)(new AddProductToCartCommand(CartId::create(), $productId));
    }

    public function testInvokeNotExistingProduct(): void
    {
        $this->expectException(NotFoundException::class);

        $cartId = self::createCart();
        self::$entityManager->flush();

        (self::$handler)(new AddProductToCartCommand($cartId, ProductId::create()));
    }

    public function testAddProduct(): void
    {
        $productId = self::createProduct();
        $cartId = self::createCart();
        self::$entityManager->flush();

        (self::$handler)(new AddProductToCartCommand($cartId, $productId));

        self::assertEquals(1, $this->findCartItemQuantity($cartId, $productId));

        (self::$handler)(new AddProductToCartCommand($cartId, $productId));

        self::assertEquals(2, $this->findCartItemQuantity($cartId, $productId));
    }

    public function testCartLimit(): void
    {
        $this->expectException(CartLimitReachedException::class);

        $productId1 = self::createProduct();
        $productId2 = self::createProduct();
        $productId3 = self::createProduct();
        $productId4 = self::createProduct();
        $cartId = self::createCart();
        self::$entityManager->flush();

        $handler = self::$handler;
        $handler(new AddProductToCartCommand($cartId, $productId1));
        $handler(new AddProductToCartCommand($cartId, $productId2));
        $handler(new AddProductToCartCommand($cartId, $productId3));
        $handler(new AddProductToCartCommand($cartId, $productId4));
        $handler(new AddProductToCartCommand($cartId, $productId4));
    }

    private static function createCart(): CartId
    {
        $id = CartId::create();
        $cart = new Cart(id: $id);

        self::$entityManager->persist($cart);

        return $id;
    }

    private static function createProduct(): ProductId
    {
        $id = ProductId::create();
        $product = new Product(
            id: $id,
            title: ':test_product:',
            price: Money::of(100, 'USD'),
        );

        self::$entityManager->persist($product);

        return $id;
    }

    public function findCartItemQuantity(CartId $cartId, ProductId $productId): int
    {
        $qb = self::$entityManager->getConnection()->createQueryBuilder();
        $qb->select('amount');
        $qb->from('cart_item');
        $qb->andWhere('cart_id = :cart_id');
        $qb->andWhere('product_id = :product_id');

        $qb->setParameter('cart_id', $cartId->value->toBinary());
        $qb->setParameter('product_id', $productId->value->toBinary());

        $quantity = $qb->fetchOne();

        Assert::integer($quantity);

        return $quantity;
    }
}
