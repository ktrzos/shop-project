<?php

declare(strict_types=1);

namespace Shop\Application\Command\Cart;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class RemoveProductFromCartCommandHandlerTest extends KernelTestCase
{
    private static RemoveProductFromCartCommandHandler $handler;
    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(RemoveProductFromCartCommandHandler::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testInvoke(): void
    {
        $cart = self::createCart();
        $product = self::createProduct();
        $cart->addProduct($product);
        self::$entityManager->flush();

        self::assertCartItemExists($product, $cart);

        (self::$handler)(new RemoveProductFromCartCommand(
            cartId: $cart->getId(),
            productId: $product->getId(),
        ));

        self::assertCartItemDoesNotExists($product, $cart);
    }

    public function testInvokeNotExisting(): void
    {
        $this->expectException(NotFoundException::class);

        (self::$handler)(new RemoveProductFromCartCommand(
            cartId: CartId::create(),
            productId: ProductId::create(),
        ));
    }

    private static function createCart(): Cart
    {
        $cart = new Cart(id: CartId::create());

        self::$entityManager->persist($cart);

        return $cart;
    }

    private static function createProduct(): Product
    {
        $product = new Product(
            id: ProductId::create(),
            title: ':test_product:',
            price: Money::of(100, 'USD'),
        );

        self::$entityManager->persist($product);

        return $product;
    }

    private static function assertCartItemExists(Product $product, Cart $cart): void
    {
        self::assertTrue(self::checkIfCartItemExists($product, $cart));
    }

    private static function assertCartItemDoesNotExists(Product $product, Cart $cart): void
    {
        self::assertFalse(self::checkIfCartItemExists($product, $cart));
    }

    public static function checkIfCartItemExists(Product $product, Cart $cart): bool
    {
        $qb = self::$entityManager->getConnection()->createQueryBuilder();
        $qb->select('id');
        $qb->from('cart_item');
        $qb->where('product_id = :product_id');
        $qb->andWhere('cart_id = :cart_id');
        $qb->setParameter('product_id', $product->getId()->value->toBinary());
        $qb->setParameter('cart_id', $cart->getId()->value->toBinary());

        $result = $qb->fetchOne();

        return !($result === false);
    }
}
