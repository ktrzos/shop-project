<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class GetProductListQueryHandlerTest extends KernelTestCase
{
    private static GetProductListQueryHandler $handler;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(GetProductListQueryHandler::class);
    }

    public function testNoPageGiven(): void
    {
        $query = new GetProductListQuery(page: null);
        $result = (self::$handler)($query);

        self::assertCount(3, $result);
    }

    public function testFirstPage(): void
    {
        $query = new GetProductListQuery(page: 1);
        $result = (self::$handler)($query);

        self::assertCount(3, $result);
    }

    public function testSecondPage(): void
    {
        $query = new GetProductListQuery(page: 2);
        $result = (self::$handler)($query);

        self::assertCount(2, $result);
    }
}
