<?php

declare(strict_types=1);

namespace Shop\Application\Query\Product;

use Doctrine\DBAL\Connection;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class GetProductByIdQueryHandlerTest extends KernelTestCase
{
    private static Connection $connection;
    private static GetProductByIdQueryHandler $handler;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$connection = self::getContainer()->get(Connection::class);
        self::$handler = self::getContainer()->get(GetProductByIdQueryHandler::class);
    }

    public function testInvokeNotExisting(): void
    {
        $query = new GetProductByIdQuery(id: ProductId::create());
        $result = (self::$handler)($query);

        self::assertNull($result);
    }

    public function testInvoke(): void
    {
        $id = $this->loadFirstProduct();

        $query = new GetProductByIdQuery(id: $id);
        $result = (self::$handler)($query);

        self::assertNotNull($result);
        self::assertEquals($id, $result->id);
    }

    private function loadFirstProduct(): ProductId
    {
        $qb = self::$connection->createQueryBuilder();
        $qb->select('id');
        $qb->from('product');
        $qb->setMaxResults(1);

        $id = $qb->fetchOne();

        Assert::string($id);

        return ProductId::fromString($id);
    }
}
