<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Domain\Exception\NotFoundException;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\CartItem;
use Shop\Domain\Entity\CartItemId;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class GetCartByIdQueryHandlerTest extends KernelTestCase
{
    private static GetCartByIdQueryHandler $handler;
    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(GetCartByIdQueryHandler::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testCartDoesNotExists(): void
    {
        $this->expectException(NotFoundException::class);

        $cartId = CartId::create();

        (self::$handler)(new GetCartByIdQuery(id: $cartId));
    }

    public function testEmptyCart(): void
    {
        $cartId = self::createCart();
        self::$entityManager->flush();

        $output = (self::$handler)(new GetCartByIdQuery(id: $cartId));

        self::assertEquals($cartId, $output->id);
        self::assertEquals(0, $output->totalPrice->getAmount()->toInt());
        self::assertCount(0, $output->itemCollection);
    }

    public function testCartWithProducts(): void
    {
        $cartId = self::createCart();
        $productId1 = self::createProduct();
        $productId2 = self::createProduct();
        self::createCartItem($cartId, $productId1);
        self::createCartItem($cartId, $productId2);
        self::$entityManager->flush();

        $output = (self::$handler)(new GetCartByIdQuery(id: $cartId));

        self::assertEquals($cartId, $output->id);
        self::assertEquals(20198, $output->totalPrice->getMinorAmount()->toInt());
        self::assertCount(2, $output->itemCollection);
    }

    private static function createCart(): CartId
    {
        $id = CartId::create();
        $cart = new Cart(id: $id);

        self::$entityManager->persist($cart);

        return $id;
    }

    private static function createProduct(): ProductId
    {
        $id = ProductId::create();
        $product = new Product(
            id: $id,
            title: ':test_product:',
            price: Money::of(100.99, 'USD'),
        );

        self::$entityManager->persist($product);

        return $id;
    }

    private static function createCartItem(CartId $cartId, ProductId $productId): void
    {
        $cart = self::$entityManager->getReference(Cart::class, $cartId->value);
        $product = self::$entityManager->getReference(Product::class, $productId->value);

        Assert::notNull($cart);
        Assert::notNull($product);

        $item = new CartItem(
            id: CartItemId::create(),
            cart: $cart,
            product: $product,
        );

        self::$entityManager->persist($item);
    }
}
