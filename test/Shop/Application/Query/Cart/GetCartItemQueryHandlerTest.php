<?php

declare(strict_types=1);

namespace Shop\Application\Query\Cart;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Entity\Cart;
use Shop\Domain\Entity\CartId;
use Shop\Domain\Entity\CartItem;
use Shop\Domain\Entity\CartItemId;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

final class GetCartItemQueryHandlerTest extends KernelTestCase
{
    private static GetCartItemQueryHandler $handler;
    private static EntityManagerInterface $entityManager;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$handler = self::getContainer()->get(GetCartItemQueryHandler::class);
        self::$entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testNotExistent(): void
    {
        $result = (self::$handler)(new GetCartItemQuery(
            cartId: CartId::create(),
            productId: ProductId::create(),
        ));

        self::assertNull($result);
    }

    public function testFinding(): void
    {
        $productId = self::createProduct();
        $cartId = self::createCart();
        $cartItemId = self::createCartItem($cartId, $productId);
        self::$entityManager->flush();

        $result = (self::$handler)(new GetCartItemQuery($cartId, $productId));

        self::assertNotNull($result);
        self::assertEquals($cartItemId->value, $result->cartItemId->value);
        self::assertEquals(':test_product:', $result->product->title);
        self::assertEquals(10000, $result->product->price->getMinorAmount()->toInt());
        self::assertEquals(1, $result->quantity);
    }

    private static function createCart(): CartId
    {
        $id = CartId::create();
        $cart = new Cart(id: $id);

        self::$entityManager->persist($cart);

        return $id;
    }

    private static function createProduct(): ProductId
    {
        $id = ProductId::create();
        $product = new Product(
            id: $id,
            title: ':test_product:',
            price: Money::of(100, 'USD'),
        );

        self::$entityManager->persist($product);

        return $id;
    }

    private static function createCartItem(CartId $cartId, ProductId $productId): CartItemId
    {
        $cartItemId = CartItemId::create();
        $cart = self::$entityManager->getReference(Cart::class, $cartId->value);
        $product = self::$entityManager->getReference(Product::class, $productId->value);

        Assert::notNull($cart);
        Assert::notNull($product);

        $item = new CartItem(
            id: $cartItemId,
            cart: $cart,
            product: $product,
        );

        self::$entityManager->persist($item);

        return $cartItemId;
    }
}
