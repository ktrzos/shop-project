<?php

declare(strict_types=1);

namespace Test\Shop\Domain\Service;

use Brick\Money\Money;
use loophp\collection\Collection;
use PHPUnit\Framework\TestCase;
use Shop\Domain\Entity\CartItemId;
use Shop\Domain\Entity\ProductId;
use Shop\Domain\Service\CartTotalCalculator;
use Shop\Domain\View\CartItemView;
use Shop\Domain\View\ProductView;

final class CartTotalCalculatorTest extends TestCase
{
    public function testCalculateTotalForEmpty(): void
    {
        $collection = Collection::fromIterable([]);

        self::assertEquals(0, CartTotalCalculator::calculateTotal($collection)->getAmount()->toFloat());
    }

    public function testCalculateTotalForOne(): void
    {
        $collection = Collection::fromIterable([
            $this->createItemView(quantity: 1, price: 100.99),
        ]);

        self::assertEquals(100.99, CartTotalCalculator::calculateTotal($collection)->getAmount()->toFloat());
    }

    private function createItemView(int $quantity, float $price): CartItemView
    {
        return new CartItemView(
            cartItemId: CartItemId::create(),
            product: new ProductView(
                id: ProductId::create(),
                title: ':product_title',
                price: Money::of($price, 'USD'),
            ),
            quantity: $quantity,
        );
    }
}
