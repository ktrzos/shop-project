<?php

declare(strict_types=1);

namespace Shop\Domain\Entity;

use Brick\Money\Money;
use Doctrine\ORM\EntityManagerInterface;
use Shop\Domain\Exception\CartItemAmountLimitReachedException;
use Shop\Domain\Exception\CartLimitReachedException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CartTest extends KernelTestCase
{
    private static EntityManagerInterface $em;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        self::$em = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testMaxItemsInCartInvariant(): void
    {
        $this->expectException(CartLimitReachedException::class);

        $cart = $this->createCart();
        $cart->addProduct($this->createProduct());
        $cart->addProduct($this->createProduct());
        $cart->addProduct($this->createProduct());
        $cart->addProduct($this->createProduct());
    }

    public function testMaxCountInvariantOfTheSameProductInCart(): void
    {
        $product = $this->createProduct();

        $this->expectException(CartItemAmountLimitReachedException::class);

        $cart = $this->createCart();
        for ($i = 1; $i <= 11; ++$i) {
            $cart->addProduct($product);
        }
    }

    public function testAddingProduct(): void
    {
        $product = $this->createProduct();
        self::$em->flush();

        $cart = $this->createCart();
        $cart->addProduct($product);

        self::$em->flush();
        self::assertProductCountInCart(1, $product, $cart);
    }

    public function testAddingTheSameProductTwice(): void
    {
        $product = $this->createProduct();

        $cart = $this->createCart();
        $cart->addProduct($product);
        $cart->addProduct($product);

        self::$em->flush();
        self::assertProductCountInCart(2, $product, $cart);
    }

    private static function assertProductCountInCart(int $count, Product $product, Cart $cart): void
    {
        $qb = self::$em->createQueryBuilder();
        $qb->select('i.amount');
        $qb->from(CartItem::class, 'i');
        $qb->andWhere('i.product = :product_id');
        $qb->andWhere('i.cart = :cart_id');
        $qb->setParameter('product_id', $product->getId()->value->toBinary());
        $qb->setParameter('cart_id', $cart->getId()->value->toBinary());

        $result = $qb->getQuery()->getSingleScalarResult();

        self::assertEquals($count, $result);
    }

    public function createProduct(): Product
    {
        $product = new Product(
            id: ProductId::create(),
            title: ':title:',
            price: Money::of(100, 'USD'),
        );

        self::$em->persist($product);

        return $product;
    }

    public function createCart(): Cart
    {
        $cart = new Cart(CartId::create());

        self::$em->persist($cart);

        return $cart;
    }
}
