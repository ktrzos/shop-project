<?php

declare(strict_types=1);

namespace Test\Shop\Domain\Entity;

use Brick\Money\Money;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Shop\Domain\Entity\Product;
use Shop\Domain\Entity\ProductId;

final class ProductTest extends TestCase
{
    public function testCreateWithInvalidCurrency(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Product(
            id: ProductId::create(),
            title: ':title:',
            price: Money::of(100, 'EUR'),
        );
    }
}
