<?php

declare(strict_types=1);

use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput as ConsoleOutput;
use Symfony\Component\Dotenv\Dotenv;

$file = __DIR__ . '/../vendor/autoload.php';

if (!file_exists($file)) {
    throw new RuntimeException('Install dependencies using Composer to run the test suite.');
}

$autoload = require $file;

(new Dotenv())->usePutenv(false)->loadEnv(dirname(__DIR__) . '/.env.test');

$application = new Application(new Kernel('test', true));
$application->setAutoExit(false);
$application->setCatchExceptions(false);

// Drop existing database
$input = new ArrayInput(['command' => 'doctrine:database:drop']);
try {
    $application->run($input, new ConsoleOutput());
} catch (Exception $e) {
    dump($e);
    echo 'Error while running `doctrine:database:drop` command.';
    exit(32);
}
echo "\n- Database dropped";

// Create database
$input = new ArrayInput(['command' => 'doctrine:database:create']);
try {
    $application->run($input, new ConsoleOutput());
} catch (Exception $e) {
    dump($e);
    echo 'Error while running `doctrine:database:create` command.';
    exit(33);
}
echo "\n- Database created";

// Create database schema
$input = new ArrayInput(['command' => 'doctrine:schema:update', '--force' => true]);
try {
    $application->run($input, new ConsoleOutput());
} catch (Exception $e) {
    dump($e);
    echo 'Error while running `doctrine:schema:update` command.';
    exit(34);
}
echo "\n- Database updated";

// Load fixtures of the AppTestBundle
$input = new ArrayInput(['command' => 'doctrine:fixtures:load', '--no-interaction' => true, '--append' => false]);
try {
    $application->run($input, new ConsoleOutput());
} catch (Exception $e) {
    echo 'Error while running `doctrine:fixtures:load` command: ' . $e->getMessage();
    exit(35);
}
echo "\n- Fixtures loaded";

unset($input, $application);

// end line
echo "\n\n";
